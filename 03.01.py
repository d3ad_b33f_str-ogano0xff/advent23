
sol = 0


def extract_ints(row):
    spec = ['-', '&', '$', '@', '%', '=', '#', '*', '+', '/']
    numz = [x for x in row.strip().split('.') if x not in spec]
    return [x for x in numz if x]


def find_flag(string_in):
    spec = ['-', '&', '$', '@', '%', '=', '#', '*', '+', '/']
    for char in string_in:
        if char in spec:
            return True
    return False


def clean_int(string_in):
    outp = ''
    for char in string_in:
        if char.isdigit():
            outp += char
    if outp:
        return int(outp)
    else:
        print('clean fail', string_in)
        return 0


def sniffr(target, data, idx):
    if find_flag(target):
        return True
    if sniff_behnd(target, data, idx):
        return True
    if sniff_ahead(target, data, idx):
        return True
    return False


def sniff_ahead(target, data, idx):
    if len(data)-1 > idx:
        src = data[idx]
        sniff = data[idx+1]
        locat = src.find(target)
        if locat > 0:
            slice = sniff[locat-1:locat + len(target) + 1]
        else:
            slice = sniff[locat:locat + len(target) + 1]
        return find_flag(slice)
    return False


def sniff_behnd(target, data, idx):
    if idx > 0:
        src = data[idx]
        sniff = data[idx-1]
        locat = src.find(target)
        if locat > 0:
            slice = sniff[locat-1:locat + len(target) + 1]
        else:
            slice = sniff[locat:locat + len(target) + 1]
        return find_flag(slice)
    return False


def sanity_snapshot(target, data, idx):
    print(target)
    src = data[idx]
    locat = src.find(target)
    if idx > 0:
        sniff = data[idx-1]
        if locat > 0:
            slice = sniff[locat - 1:locat + len(target) + 1]
        else:
            slice = sniff[locat:locat + len(target) + 1]
        print('0\t', slice)

    print('1\t', src[locat-1:locat + len(target) + 1])

    if len(data)-1 > idx:
        sniff = data[idx+1]
        if locat > 0:
            slice = sniff[locat - 1:locat + len(target) + 1]
        else:
            slice = sniff[locat:locat + len(target) + 1]
        print('2\t', slice)

    print('Valid? ', target, clean_int(target), sniffr(target, data, idx))


with open('03.01.txt', 'r') as opfil:
    inp = opfil.readlines()

for lnn, dat in enumerate(inp):
    numz = extract_ints(dat)
    print(dat)
    print(numz)

    for num in numz:

        if sniffr(num, inp, lnn):
            sol += clean_int(num)

        sanity_snapshot(num, inp, lnn)

print('\n\nsolve?', sol == 549908)

