with open('05.01.txt', 'r') as opfil:
    seeds = opfil.readline().strip().split(': ')[1].split(' ')
    dat = opfil.readlines()


def convert(flag, hunt, source):
    for idx, lnn in enumerate(source):
        if flag not in lnn:
            continue
        target = idx + 1
        while '-to-' not in source[target]:
            dstr, srcr, incr = [int(x) for x in source[target].split(' ')]
            if srcr <= hunt < srcr+incr:
                return dstr
            target += 1
    print(flag, hunt, 'not mapped')
    return hunt


seeds = [int(x) for x in seeds]
seeddict = {}

dat = [x.strip() for x in dat if x.strip()]

lowest_loc = 999999999999999999

for each in seeds:
    soil = convert('seed-to-soil', each, dat)
    fert = convert('soil-to-fertilizer', soil, dat)
    watr = convert('fertilizer-to-water', fert, dat)
    ligh = convert('water-to-light', watr, dat)
    temp = convert('light-to-temperature', ligh, dat)
    humd = convert('temperature-to-humidity', temp, dat)
    loca = convert('humidity-to-location', humd, dat)
    if loca < lowest_loc:
        lowest_loc = loca
    seeddict[each] = {
        'soil': soil,
        'fertilizer': fert,
        'water': watr,
        'light': ligh,
        'temperature': temp,
        'humidity': humd,
        'location': loca
    }
a = 1
