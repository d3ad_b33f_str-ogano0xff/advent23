with open('08.01.txt', 'r') as opfil:
    instr = opfil.readline().strip()
    br = opfil.readline()
    inp = opfil.readlines()

convert = {}

for each in inp:
    convert[each[:3]] = {'l': each[7:10], 'r': each[12:15]}

grab = 'AAA'
ct = 0

while grab != 'ZZZ':
    for instruction in instr:
        ct += 1
        #print(grab, instruction, convert[grab][instruction.lower()])
        grab = convert[grab][instruction.lower()]
    print(grab, ct)
