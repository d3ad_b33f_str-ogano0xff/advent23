import re

outp = 0

max = {'red': 12, 'green': 13, 'blue': 14}

with open('02.01.txt', 'r') as opfil:
    games = re.findall('Game ([0-9]+): ([0-9 a-z ,:; ]+)', opfil.read())

for game in games:
    print(game)
    possible = True
    for cycle in game[1].split('; '):
        for color in cycle.split(', '):
            ct, clr = re.findall('([0-9]+) ([a-z]+)', color)[0]
            if int(ct) > max[clr]:
                possible = False
    if possible:
        outp += int(game[0])

print(outp)