with open('04.01.txt', 'r') as opfil:
    inp = opfil.readlines()

allpoints = 0
bonuses = []

def tally_scratch(row):
    points = 0
    goodnums, allnums = row.strip().split(': ')[1].split(' | ')
    goodnums = goodnums.split(' ')
    goodnums = [int(x) for x in goodnums if x]
    allnums = allnums.split(' ')
    allnums = [int(x) for x in allnums if x]

    for num in goodnums:
        if num in allnums:
            if points:
                points *= 2
            else:
                points = 1
    return points


for row in inp:
    allpoints += tally_scratch(row)

print(allpoints)
