with open('04.01.txt', 'r') as opfil:
    inp = opfil.readlines()

allpoints = 0
bonuses = []
cards = {}

def tally_scratch(row):
    points = 0
    cardnm = row.strip().split(': ')[0].split(' ')[-1]
    goodnums, allnums = row.strip().split(': ')[1].split(' | ')
    goodnums = goodnums.split(' ')
    goodnums = [int(x) for x in goodnums if x]
    allnums = allnums.split(' ')
    allnums = [int(x) for x in allnums if x]
    goodnums.sort()
    allnums.sort()

    matchnums = []

    for num in goodnums:
        if num in allnums:
            matchnums.append(num)
            if points:
                points *= 2
            else:
                points = 1
    print(cardnm)
    cards[int(cardnm)] = {
        'ct': 1,
        'matches': len(matchnums)
    }

    return points, len(matchnums)


all_ct = 0

for idx, row in enumerate(inp):
    points, match = tally_scratch(row)
    all_ct += match + 1

for card in cards:
    bonuses = cards[card]['matches']
    if bonuses:
        for iter in range(cards[card]['ct']):
            for incr in range(1, cards[card]['matches']+1):
                cards[card+incr]['ct'] += 1

print(sum([cards[x]['ct'] for x in cards]))
