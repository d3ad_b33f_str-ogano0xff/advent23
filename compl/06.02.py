with open('06.01.txt', 'r') as opfil:
    time = opfil.readline().strip().split(': ')[1].split(' ')
    dist = opfil.readline().strip().split(': ')[1].split(' ')
    time = [x for x in time if x]
    dist = [x for x in dist if x]
    time = [int(''.join(time))]
    dist = [int(''.join(dist))]

ct = 1
sol = 1

for each in zip(time, dist):
    winz = 0
    print(each)
    for sec_hold in range(each[0]+1):
        race_nm = ct
        speed   = sec_hold
        ttl     = each[0]-sec_hold
        distanc = sec_hold * (each[0]-sec_hold)
        if distanc > each[1]:
            winz += 1
            #print(f'Race: {race_nm} - Speed: {speed} - TTL: {ttl} - Distance: {distanc}')
    sol = winz * sol
    ct += 1

print(sol)