with open('07.01.txt', 'r') as opfil:
    dat = opfil.readlines()
    hands = [x.strip().split(' ') for x in dat]

dat = {
    3: [],
    4: [],
    5: [],
    6: [],
    7: [],
    8: [],
    9: [],
}

xlate_score = {
    3: 'high card',
    4: 'one pair',
    5: 'two pair',
    6: 'three of a kind',
    7: 'full house',
    8: 'four of a kind',
    9: 'five of a kind',
}

sol = 0


def score_card(string):
    uniq_ct = len(set(string))

    if uniq_ct == 5:
        # high card
        return 3

    elif uniq_ct == 1:
        # five of a kind
        return 9

    elif uniq_ct == 2:
        for char in set(string):
            if string.count(char) == 4:
                # four of a kind
                return 8
        # full house
        return 7

    elif uniq_ct == 3:
        for char in set(string):
            if string.count(char) == 3:
                # three of a kind
                return 6
        # two pair
        return 5

    else:
        # one pair
        return 4


for hand in hands:
    score = score_card(hand[0])
    dat[score].append({'hand': hand[0], 'bid': int(hand[1]), 'score': score})

mysort = '23456789TJQKA'

for each in dat:
    sorted_data = sorted(dat[each], key=lambda word: [mysort.index(c) for c in word['hand']])
    with open('07.01.sort', 'a') as outf:
        for lnn in sorted_data:
            outf.write('{} {} {}'.format(lnn['hand'], lnn['bid'], xlate_score.get(lnn['score'])))
            outf.write('\n')

with open('07.01.sort', 'r') as infil:
    sol = 0
    data = infil.readlines()
    for idx, dat in enumerate(data):
        thisscore = (idx+1)*int(dat.strip().split(' ')[1])
        sol += thisscore

print(sol)
