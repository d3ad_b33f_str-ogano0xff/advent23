import json

with open('07.01.txt', 'r') as opfil:
    dat = opfil.readlines()
    hands = [x.strip().split(' ') for x in dat]

dat = {
    3: [],
    4: [],
    5: [],
    6: [],
    7: [],
    8: [],
    9: [],
}

xlate_score = {
    3: 'high card',
    4: 'one pair',
    5: 'two pair',
    6: 'three of a kind',
    7: 'full house',
    8: 'four of a kind',
    9: 'five of a kind',
}

sol = 0


def score_card(string):
    uniq_ct = len(set(string))

    if uniq_ct == 5:
        # high card
        return 3

    elif uniq_ct == 1:
        # five of a kind
        return 9

    elif uniq_ct == 2:
        for char in set(string):
            if string.count(char) == 4:
                # four of a kind
                return 8
        # full house
        return 7

    elif uniq_ct == 3:
        for char in set(string):
            if string.count(char) == 3:
                # three of a kind
                return 6
        # two pair
        return 5

    else:
        # one pair
        return 4


def iter_jokers(string):
    alts = [string]

    if string.count('J') == 5:
        return 'AAAAA', score_card('AAAAA')

    for x in range(5):
        for alt in alts:
            if 'J' in alt:
                for scrub in iter_alts(alt, alt.find('J')):
                    alts.append(scrub)

    alts = [x for x in list(set(alts)) if 'J' not in x]

    score = 0
    highp = '.....'

    for alt in alts:
        count = score_card(alt)
        if count > score:
            score = count
            highp = alt

    return highp, score


def iter_alts(string, idx):
    for each in '23456789TQKA':
        yield '{}{}{}'.format(string[:idx], each, string[idx+1:])


for hand in hands:
    calc = False
    besthand = hand[0]
    score = score_card(hand[0])
    if 'J' in hand[0]:
        calc = True
        besthand, score = iter_jokers(hand[0])
    dat[score].append({'hand': besthand, 'bid': int(hand[1]), 'score': score, 'original': hand[0], 'calc': calc})
    print(hand[0], besthand, score)

mysort = 'J23456789TQKA'

for each in dat:
    sorted_data = sorted(dat[each], key=lambda word: [mysort.index(c) for c in word['original']])
    with open('07.02.sort', 'a') as outf:
        for lnn in sorted_data:
            outf.write(json.dumps(lnn))
            outf.write('\n')

with open('07.02.sort', 'r') as infil:
    sol = 0
    data = infil.readlines()
    for idx, dat in enumerate(data):
        lnn = json.loads(dat)
        thisscore = (idx+1)*int(lnn['bid'])
        sol += thisscore

print(sol)
